//
//  Person.h
//  Frans_CoreData
//
//  Created by Vensi Developer on 12/17/13.
//  Copyright (c) 2013 Frans. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (retain,strong) NSString *firstName;

@property (retain,strong) NSString *lastName;   

@end
