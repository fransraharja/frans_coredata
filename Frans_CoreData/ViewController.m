//
//  ViewController.m
//  Frans_CoreData
//
//  Created by Vensi Developer on 12/17/13.
//  Copyright (c) 2013 Frans. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()
{
    NSManagedObjectContext *context;
}

@end

@implementation ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self firstnameTextField]setDelegate:self];
    [[self lastnameTextField]setDelegate:self];
    
    AppDelegate *appdelegate = [[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addPerson:(id)sender {
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:context];
    NSManagedObject *newPerson = [[NSManagedObject alloc]initWithEntity:entitydesc insertIntoManagedObjectContext:context];
    
    [newPerson setValue:self.firstnameTextField.text forKey:@"firstname"];
    [newPerson setValue:self.lastnameTextField.text forKey:@"lastname"];
    
    NSError *error;

    NSPersistentStoreCoordinator *storeCoordinator = [[NSPersistentStoreCoordinator alloc]init];
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSURL *filePath = [NSURL fileURLWithPath:[basePath stringByAppendingPathComponent: @"Frans_CoreData.sqlite"]];
    NSPersistentStore *firstStore = [storeCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:filePath options:nil error:&error];
    [context assignObject:newPerson toPersistentStore:firstStore];
    self.displayLabel.text = @"Person Added";
}

- (IBAction)searchPerson:(id)sender {
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstname like %@ and lastname like %@",self.firstnameTextField.text,self.lastnameTextField.text];
    [request setPredicate:predicate];
    
    NSError *error;
    
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if(matchingData.count <= 0)
    {
        self.displayLabel.text = @"No Person Found";
    }
    else{
        NSString *firstName;
        NSString *lastName;
        
        for(NSManagedObject *obj in matchingData)
        {
            firstName = [obj valueForKey:@"firstname"];
            lastName = [obj valueForKey:@"lastname"];

        }
        self.displayLabel.text = [NSString stringWithFormat:@"Firstname %@, Lastname %@",firstName,lastName];

    }
}

- (IBAction)deletePerson:(id)sender {
    
    NSEntityDescription *entitydesc = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entitydesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstname like %@ and lastname like %@",self.firstnameTextField.text,self.lastnameTextField.text];
    [request setPredicate:predicate];
    
    NSError *error;
    
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if(matchingData.count <= 0)
    {
        self.displayLabel.text = @"No Person Deleted";
    }
    else{
        int count = 0;
        for (NSManagedObject *obj in matchingData) {
            [context deleteObject:obj];
            ++count;
        }
        [context save:&error];
        self.displayLabel.text = [NSString stringWithFormat:@"%d Person Deleted", count];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}
@end
