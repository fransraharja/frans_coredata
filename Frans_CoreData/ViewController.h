//
//  ViewController.h
//  Frans_CoreData
//
//  Created by Vensi Developer on 12/17/13.
//  Copyright (c) 2013 Frans. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *firstnameTextField;

@property (strong, nonatomic) IBOutlet UITextField *lastnameTextField;

@property (strong, nonatomic) IBOutlet UITextView *displayLabel;

- (IBAction)addPerson:(id)sender;
- (IBAction)searchPerson:(id)sender;
- (IBAction)deletePerson:(id)sender;



@end
